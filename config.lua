-- This file contains options used to configure tsm_pyramids actions
pyramids.options = {

	-- Ground types : desert sand, group sand ...
	ground_type = {"group:sand"},  --{"default:desert_sand"},
	-----------------------------
	-- Mob and spawner
	-----------------------------
	-- Create a mummy entity and an assosciated spawner
	register_included_mummy	= false,
	-- Place mob (mummy) and spawners in pyramids
	use_mummy_and_spawner = true,
	-- Spawn a custom mob instead of the included mummy
	-- ( set to nil to use the included mummy )
	custom_mob	= "creatures:mummy",
	-- Place a custom spawner instead of the included one
	-- ( set to nil to use the included spawner )
	custom_spawner	= "creatures:mummy_spawner",

	-----------------------------
	-- Traps
	-----------------------------
	-- Niche in the wall (make the spawner is visible)
	niche_in_the_wall	= true,
	-- Create a traps in pyramids
	use_traps			= true,
	-- The is lava under the pyramids (as part of the traps)
	use_lava			= false,

	-----------------------------
	-- Chests
	-----------------------------
	-- Chances of an empty chest to be placed
	empty_chest_probability	= 5, -- out of 10 
	
	-- Items in filled chests
	items_in_chests		= {
		-- Minimum number of items in one chest
		num_min				=	3,
		-- Maximum number of items in one chest
		num_max				=	15,
		-- Categories of items present in chests
		-- (nil means all types)
		items_types			=	{	"ranged_weapon",
									"melee_weapon",
									"minetool",
									"food",
									"crafting_component",
									"default",
									},
		-- Minimum preciousness of items
		preciousness_min	= 	1,
		-- Maximum preciousness of items
		preciousness_max	=	7,
		},
}
